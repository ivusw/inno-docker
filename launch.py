#!/usr/bin/python

import argparse
import glob
import os
import re
import subprocess
import sys
import time
from datetime import datetime

if sys.version_info[0] >= 3:
    import urllib.request
else:
    import urllib2

my_path = os.path.dirname(os.path.realpath(__file__))
my_cwd = os.path.realpath(os.getcwd())

class internet:
    result = None
    @classmethod
    def have_internet(cls):
        if cls.result is not None:
            return cls.result
        try:
            import httplib
        except:
            import http.client as httplib

        conn = httplib.HTTPConnection("52.218.218.0", timeout=2)
        try:
            conn.request("HEAD", "/")
            conn.close()
            cls.result = True
        except:
            conn.close()
            cls.result = False
        return cls.result

def fetch_deb(args):
    if args.public_build:
        if args.backtrace:
            ppp = 'psymbol'
        else:
            ppp = 'public'
    else:
        ppp = 'internal'
    ver = args.tag
    deb_file = "ros-{ros_version}-innovusion-driver-{ver}-{ppp}.deb".format(ros_version=args.ros_version,
                                                                            ppp=ppp,
                                                                            ver=ver)
    if os.path.isfile(deb_file):
        print("{} is already there".format(deb_file))
        statinfo = os.stat(deb_file)
        if statinfo.st_size < 100000:
            print("deb file {} is too small: {}".format(deb_file, statinfo.st_size))
            exit(1)
    else:
        print("deb file {} is not there.".format(deb_file))
        exit(1)
    return deb_file

def get_deb(args):
    if args.deb_file:
        deb_file = args.deb_file
    else:
        deb_file = fetch_deb(args)

    if not os.path.isfile(deb_file):
        print("deb file {} doesn't exist".format(deb_file))
        exit(1)

    statinfo = os.stat(deb_file)
    if statinfo.st_size < 100000:
        print("deb file {} is invalid: {}".format(deb_file, statinfo.st_size))
        exit(1)
    return deb_file

def get_map_v(src, map_prefix, need_create=False):
    abs_path = os.path.abspath(src)
    mapped_path = os.path.join(map_prefix, os.path.basename(os.path.normpath(abs_path)))
    map_vol = ' -v={}:{} '.format(abs_path, mapped_path)
    if not os.path.exists(abs_path):
        if need_create:
            os.makedirs(abs_path)
        else:
            print("{} doesn't exist".format(abs_path))
            exit(1)
    return mapped_path, map_vol

def has_nvidia_docker():
    cmd = "nvidia-docker version"
    try:
        _ = subprocess.check_output(cmd, shell=True)
        return True
    except Exception as e:
        # print(e)
        print("No nvidia-docker")
        return False

def exec_lidar_util(deb_file, path, args):
    cmd = "{} {} {} {}".format(os.path.join(path, "exec-deb-lidar-util.bash"),
                               deb_file, args.ros_version,
                               ' '.join(args.lidar_util))
    try:
        if (args.hard_stop):
            cmd = "/usr/bin/timeout {} {}".format(args.hard_stop, cmd)
        print(cmd)
        output = subprocess.check_output(cmd,
                                         stderr=subprocess.STDOUT,
                                         shell=True,
                                         universal_newlines=True)
        ret = 0
    except subprocess.CalledProcessError as exc:
        output = exc.output
        ret = exc.returncode
    if (len(output)):
        print(output)
        return 0
    else:
        return ret

def get_bag_filename(args):
    rosbag_args = args.record_bag_file

    # does rosbag_args include '-o <prefix>' or '--output-prefix <prefix>'?  If so, use the
    # prefix and append datetime.bag.  If not, just use datetime.bag.
    bag_filename = "{}".format(script_start)
    if ("-o" in rosbag_args) or ("--output-prefix" in rosbag_args):
        rosbag_arglist = rosbag_args.split()
        rosbag_args = ""
        skip_next = False
        for i in range(len(rosbag_arglist)):
            if not skip_next:
                arg = rosbag_arglist[i]
                if ("-o" == arg) or ("--output-prefix" == arg):
                    i +=1
                    bag_filename = "{}_{}".format(rosbag_arglist[i], bag_filename)
                    skip_next = True
                else:
                    rosbag_args = "{} {}".format(rosbag_args, arg)
            else:
                skip_next = False

    rosbag_args = "--output-name {} {}".format(bag_filename, rosbag_args)
    return (bag_filename, rosbag_args)


def get_record_bag_cmd(args):
    if args.record_bag_file is not None:
        cmd_record_bag_file = '(echo start rosbag record iv_points; cd /root/output;'
        rosbag_args = get_bag_filename(args)[1]
        if args.process_frames:
            rosbag_args = '--limit={} {}'.format(args.process_frames, rosbag_args)
        cmd_record_bag_file += 'rosbag record iv_points {};'.format(rosbag_args)
        cmd_record_bag_file += ' echo rosbag record done) & '
    else:
        cmd_record_bag_file = ''

    return cmd_record_bag_file

def post_process_bag(args):
    # innovusion_pointcloud understands arguments skip_frames and process_frames starting @ 1.1.3-rc15.
    # So we need to do rosbag filtering if skip_frames is desired and an earlier version is used.
    ret_cmd = ""
    if args.record_bag_file:
        bag_filename = get_bag_filename(args)[0]
        bag_in = "output/{}.bag".format(bag_filename)
        bag_active = "{}.active".format(bag_in)
        if not os.path.isfile(bag_in) and os.path.isfile(bag_active):
            print("Reindexing to repair likely bag file corruption\n=====\n")
            ret_cmd = "cd; mv {} {}; rosbag reindex {}; ".format(bag_active, bag_in, bag_in)

        need_postprocess = False
        if args.skip_frames:
            if args.tag is not None:
                deb_ver = args.tag
            else:
                latest_tag_file = 'LATEST_MFG_TAG'
                with open(latest_tag_file) as f:
                    first_line = f.readline()
                    deb_ver = first_line.strip()

            ver_parts = re.split("-", deb_ver)
            maj_min_build = re.split("\.", ver_parts[1])
            print("major:{}, minor:{}, build:{}".format(maj_min_build[0],maj_min_build[1],maj_min_build[2]))

            need_postprocess = True
            if int(maj_min_build[0]) < 2 and int(maj_min_build[1]) < 2 and int(maj_min_build[2]) < 4:
                if len(ver_parts) > 2:
                    release_candidate = re.search("rc([\d]+)", ver_parts[2])
                    print("release candidate:{}".format(release_candidate.group(1)))
                    if int(release_candidate.group(1)) > 14:
                        need_postprocess = False
            else:
                need_postprocess = False


        if need_postprocess:
            if ret_cmd == "":
                ret_cmd = "cd; "

            print("=====\nPost-processing the bag file....")

            bag_out = "output/{}_filtered.bag".format(bag_filename)
            frame_disp = "frame" if args.skip_frames == 1 else "frames"
            print("Skipping first {} {} from {}\n\tand writing to {}\n=====\n".format(
                args.skip_frames, frame_disp, bag_in, bag_out)
                )
            ret_cmd += "rosbag filter {} {} \"m.header.seq > {}\"".format(bag_in, bag_out, args.skip_frames-1)

    return ret_cmd

def main(args, use_public_build, internal_yaml_file_specified):
    if isinstance(args.list_tags, list):
        return list_tags(args.list_tags, False)
    if isinstance(args.list_tags_local, list):
        return list_tags(args.list_tags_local, True)

    home = os.path.expanduser("~")
    map_vol = ""
    lidar_util_and_ip = "/opt/ros/{}/lib/innovusion_pointcloud/innovusion_lidar_util {}".format(args.ros_version, args.lidar_ip)

    # map curent path to root
    # map_vol += "-v={}:/root".format(os.path.realpath("."))

    #deb
    if args.bag:
        mapped_deb_file = ""
        pass
    else:
        deb_file = get_deb(args)
        mapped_deb_file, mapv = get_map_v(deb_file, "/root", False)
        map_vol += mapv

    if args.lidar_util is not None:
        ret = exec_lidar_util(deb_file, my_path, args)
        return ret

    # bag
    if args.bag:
        mapped_bag, mapv = get_map_v(args.bag, "/root", False)
        map_vol += mapv

    # map launch-docker.py path
    mapped_my_path, mapv = get_map_v(my_path, "/root", False)
    map_vol += mapv
    map_vol += ' -v={}:{} '.format(my_path, my_path)

    # map cwd in necessary
    if my_path != my_cwd:
        mapped_my_cwd, mapv = get_map_v(my_cwd, "/root", False)
        map_vol += mapv
        map_vol += ' -v={}:{} '.format(my_cwd, my_cwd)
    else:
        mapped_my_cwd = mapped_my_path

    # yaml
    mapped_internal_yaml = None
    mapped_public_yaml = None
    if args.yaml:
        if internal_yaml_file_specified:
            print("use internal yaml file {}".format(args.yaml))
            mapped_internal_yaml, mapv = get_map_v(args.yaml, "/root", False)
        else:
            print("use public yaml file {}".format(args.yaml))
            mapped_public_yaml, mapv = get_map_v(args.yaml, "/root", False)

        map_vol += mapv
    else:
        mapped_public_yaml = None
        mapped_internal_yaml = None

    # rviz and ros
    rviz_dir = '{}/.rviz_{}'.format(home, args.ros_version)
    if not os.path.exists(rviz_dir):
        os.makedirs(rviz_dir)
    dest_rviz = os.path.join(rviz_dir, "default.rviz")
    if not os.path.isfile(dest_rviz):
        source_rviz = os.path.join(my_cwd, "default.rviz")
        import shutil
        shutil.copyfile(source_rviz, dest_rviz)
    map_vol += '-v="{}:/root/.rviz" '.format(rviz_dir)
    ros_dir = '{}/.ros_{}'.format(home, args.ros_version)
    map_vol += '-v="{}:/root/.ros" '.format(ros_dir)
    for d in [rviz_dir, ros_dir]:
        if not os.path.exists(d):
            os.makedirs(d)

    if args.csv_kvp_file:
        mapped_csv_kvp_file, mapv = get_map_v(args.csv_kvp_file, "/root", False)
        map_vol += mapv

    # backtrace
    if args.backtrace:
        mapped_backtrace, mapv = get_map_v(args.backtrace, "/root", False)
        map_vol += mapv
        mapped_backtrace_decoder, mapv = get_map_v(os.path.join(my_path, 'backtrace-decoder.py'), "/root", False)
        map_vol += mapv

    # lidarhome
    if not args.lidarhome:
        args.lidarhome = os.path.join(home, ".innovusion")
    mapped_lidarhome, mapv = get_map_v(args.lidarhome, "/root", True)
    map_vol += mapv

    # output
    mapped_output, mapv = get_map_v(args.output, "/root", True)
    map_vol += mapv

    # pcap
    if args.pcap:
        mapped_pcap, mapv = get_map_v(args.pcap, "/root", False)
        map_vol += mapv

    # reconfigure
    if args.reconfigure:
        cmd_reconfigure = '(echo start rqt_reconfigure; until rostopic list 2>/dev/null; do sleep 0.2; done; while !(rosparam list|grep -q p_offset); do sleep 0.2; done; rosrun rqt_reconfigure rqt_reconfigure; echo rqt_reconfigure done) & '
        if mapped_internal_yaml:
            cmd_reconfigure += '(echo start dynamic_reconfigure; until rostopic list 2>/dev/null; do sleep 0.2; done; until [ -f {} ] ; do sleep 0.2; done; rosrun dynamic_reconfigure dynparam load {} {} ; echo dynamic_reconfigure done) & '.format(mapped_internal_yaml, "/innovusion_nodelet_manager_cloud", mapped_internal_yaml)
        _, mapv = get_map_v(args.reconfigure, "/root/reconfigure", False)
        map_vol += mapv
        cmd_reconfigure += '(echo start rqt_logger_level; until rostopic list 2>/dev/null; do sleep 0.2; done; rosrun rqt_logger_level rqt_logger_level; echo rqt_logger_level done) & '
    else:
        cmd_reconfigure = ""

    cmd_install = ''
    cmd_install += 'dpkg -i "{}" && '.format(mapped_deb_file)
    cmd_install += 'echo dpkg install done && '
    if (not args.backtrace) and (args.lidar_util is None):
        cmd_install += 'until rostopic list 2>/dev/null; do sleep 0.2; done;'

    cmd_record_bag_file = get_record_bag_cmd(args)
    cmd_roscore = '(echo start roscore; roscore; echo roscore done) & '
    cmd_xterm = ''#'(echo start xterm; xterm; echo xterm done) & '
    cmd_rviz = '(echo start rviz; until rostopic list 2>/dev/null; do sleep 0.2; done; rosrun rviz rviz -f innovusion; echo rviz done) & '
    cmd_roslaunch = 'roslaunch innovusion_pointcloud innovusion_points.launch '
    cmd_main = cmd_roslaunch
    cmd_csv = "cd /root/output; /opt/ros/{}/lib/innovusion_pointcloud/{} --speed {} --ps32full {} ".format(args.ros_version,
                                                                                                           "pointcloud_server_websockets_static",
                                                                                                           args.csv_speed,
                                                                                                           args.csv_option)
    if mapped_internal_yaml:
        cmd_main = 'while !(rosnode list|grep -q dynparam); do sleep 0.2; done;' + cmd_main

    # pass arguments through to roslaunch
    if args.unit:
        cmd_main += ' unit:={} '.format(args.unit)
    if args.model:
        cmd_main += ' model:={} '.format(args.model)
        cmd_csv += '-m {} '.format(args.model)
    else:
        cmd_csv += '-m g '
    if mapped_public_yaml:
        cmd_main += ' calibration:={yaml} '.format(yaml=mapped_public_yaml)
        cmd_csv +=  ' -y {yaml} '.format(yaml=mapped_public_yaml)
    cmd_main += ' device_ip:={} '.format(args.lidar_ip)
    cmd_main += ' lidarhome:=/{} '.format(mapped_lidarhome)

    if args.processed:
        cmd_main += " processed:=1 port:=8010 "
 
    if args.record_raw:
        cmd_main += ' record:={}/{} '.format(mapped_output, args.record_raw)
        if args.record_raw_size != -1:
            cmd_main += ' record_size:={} '.format(args.record_raw_size)
    if args.pcap:
        cmd_main += ' pcap:={} '.format(mapped_pcap)
        if args.record_bag_file is not None:
            cmd_main += ' read_once:=1 '
        else:
            cmd_main += ' read_once:=0 '
        cmd_csv +=  '-f {} '.format(mapped_pcap)
        if args.record_csv_file is not None:
            if args.record_csv_file:
                n = args.record_csv_file
            else:
                n, _ = os.path.splitext(os.path.basename(args.pcap))
                n += '.csv'
            cmd_csv += '--output-csv ' + n + ' '
            if args.csv_kvp_file:
                cmd_csv += '--config {} '.format(mapped_csv_kvp_file)
            if args.multi_return_mode >= 0:
                cmd_csv += '--multi-return-mode {} '.format(args.multi_return_mode)
        else:
            cmd_csv = ''
    else:
        if args.record_csv_file is not None:
            print("can only do csv record in file mode")
            sys.exit(1)

    if args.driver_option:
        cmd_main += args.driver_option + " "

    if args.csv_kvp_file:
        kvp_nvs = []
        with open(args.csv_kvp_file) as fp:
            lines = fp.readlines()
            for line in lines:
                line = line.split("#")[0]
                line = line.split("=")
                if len(line) >= 2:
                    name = line[0].strip().rstrip()
                    value = line[1].strip().rstrip()
                    if not name.startswith("pcs_"):
                        kvp_nvs.append("{}={}".format(name, value))
                        print("from kvp file: {}".format(kvp_nvs[-1]))
                    else:
                        print("ignore pcs config from kvp file: {}".format(name))
        if len(kvp_nvs) > 0:
            cmd_main = cmd_main + ' name_value_pairs:="{}" '.format(",".join(kvp_nvs))
    if args.keep_retry:
        cmd_main = cmd_main + ' continue_live:=1 '

    if args.skip_frames:
        cmd_main = cmd_main + ' skip_frames:={} '.format(args.skip_frames)
    if args.process_frames:
        cmd_main = cmd_main + ' process_frames:={} '.format(args.process_frames)
        if not args.timer:
            run_time = 120
            run_time += args.process_frames
            if (args.skip_frames):
                run_time += args.skip_frames
            args.timer = run_time / 10

    cmd_record_raw = " cd /root/output ; {} start {} {}".format(lidar_util_and_ip, args.record_raw, args.record_raw_size)
    cmd = ""
    cmd += cmd_roscore

    show_rviz = args.record_csv_file is None
    if show_rviz:
        cmd += cmd_rviz
        cmd += cmd_record_bag_file

    cmd += cmd_xterm + cmd_reconfigure

    if args.backtrace:
        cmd = cmd_install
        cmd += " {} --base-dir /opt/ros {} ".format(mapped_backtrace_decoder, mapped_backtrace)
    elif args.bag:
        cmd_bag = " rosbag reindex {} ; rosbag play -l {} ".format(mapped_bag, mapped_bag)
        cmd += cmd_bag
    else:
        cmd += cmd_install
        if args.debug:
            cmd += ' ( echo source /opt/ros/{}/setup.bash >> ~/.bashrc ; bash ) '.format(args.ros_version)
        elif args.ros_master:
            uri = args.ros_master
            if not uri.startswith("http://"):
                uri = "http://{}".format(uri)
            if not uri.endswith(":11311"):
                uri = "{}:11311".format(uri)
            cmd_master = 'export ROS_MASTER_URI={}'.format(uri)
            cmd_hosts = 'echo {} nuc >> /etc/hosts'.format(args.ros_master)
            cmd_debug = 'echo source /opt/ros/{}/setup.bash >> ~/.bashrc ; bash'.format(args.ros_version)
            cmd = '{} ; {} {} & {} {} ; {}'.format(
                cmd_master, cmd_roscore, cmd_roslaunch, cmd_rviz, cmd_hosts, cmd_debug)
        else:
            if show_rviz:
                cmd += cmd_main
            else:
                if args.record_csv_file is None:
                    cmd += cmd_record_raw
                else:
                    cmd += cmd_csv

    if args.timer:
        mapped_timer, mapv = get_map_v(os.path.join(my_path, '../../mis/scripts/timed_stop.bash'), "/root", False)
        map_vol += mapv
        run_time = args.timer
        if run_time > 0:
            cmd += ' --pid=~/roslaunch.pid & {} {}'.format(mapped_timer, run_time)

    if not has_nvidia_docker():
        docker_cmd = 'docker'
    else:
        docker_cmd = 'nvidia-docker'

    docker_image = "{}:{}".format(args.docker_repo, args.docker_tag)

    docker_pull_cmd = "docker pull {}".format(docker_image)
    if internet.have_internet():
        print("update container image: {}".format(docker_pull_cmd))
        subprocess.call(docker_pull_cmd, shell=True)
    else:
        print("no internet, use current image {}".format(docker_image))

    docker_run_cmd = 'xhost +local:root; ' +\
                     '{} run {} --rm '.format(docker_cmd, '-t' if args.non_interactive else '-it') +\
                     '--label rosdocker{} '.format(args.label) +\
                     '--env="DISPLAY" ' +\
                     '--env="QT_X11_NO_MITSHM=1" ' +\
                     '--env="IV_CWD={}" '.format(my_cwd) +\
                     '{map_vol} '+\
                     '--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" ' +\
                     '{docker_image} ' +\
                     'bash -c "{cmd}"'
    docker_run_cmd = docker_run_cmd.format(map_vol=map_vol, docker_image=docker_image, cmd=cmd)
    print("docker launch command: {}".format(docker_run_cmd))
    print("=====\ninside docker command: {}".format(cmd))
    print("=====\n'main' command: {}".format(cmd_main))
    return_code = subprocess.call(docker_run_cmd, shell=True)

    post_process_cmd = post_process_bag(args)
    if post_process_cmd:
        lite_docker_run_cmd = '{} run {} --rm '.format(docker_cmd, '-t' if args.non_interactive else '-it') +\
                        '--label rosdocker{} '.format(args.label) +\
                        '{map_vol} '+\
                        '{docker_image} ' +\
                        'bash -c "{cmd}"'
        lite_docker_run_cmd = lite_docker_run_cmd.format(map_vol=map_vol, docker_image=docker_image, cmd=post_process_cmd)
        return_code += subprocess.call(lite_docker_run_cmd, shell=True)
        print("Done!!!!")
    return return_code

class S3:
    def __init__(self, session, bucket):
        self.bucket = bucket
        try:
            self.client = session.client('s3')
        except Exception as err:
            print("Failed to create boto3 client use profle {}".format(err))
            sys.exit(1)

    def is_file_exist(self, key):
        from botocore.exceptions import ClientError
        try:
            self.client.head_object(Bucket=self.bucket, Key=key)
            return True
        except ClientError as e:
            print(e, key, self.bucket)
            return False

    def download(self, local_file, key):
        from botocore.exceptions import ClientError
        try:
            if not self.is_file_exist(key):
                return False
            with open(local_file, 'wb') as data:
                self.client.download_fileobj(Bucket=self.bucket, Key=key, Fileobj=data)
            print("{} downloaded".format(local_file))
        except ClientError as err:
            print("Failed to download file to S3 {}. {}".format(key, str(err)))
            return False
        except IOError as err:
            print("Failed to access {} in this directory. {}".format(local_file, str(err)))
            return False
        return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--tag", type=str, default="", help="git repo tag or commit-id")
    parser.add_argument("--deb-file", type=str, default="", help="deb file path")
    parser.add_argument("--ros-version", type=str, default="kinetic", choices=["indigo", "kinetic", "melodic"], help="ros version, default is kinetic")
    parser.add_argument("--public-build", dest='public_build', action="store_true", help="use public build", default=True)
    parser.add_argument("--internal-build", dest='public_build', action="store_false", help="use internal build", default=True)
    parser.add_argument("--debug", action="store_true", help="debug, no install and launch driver", default=False)
    parser.add_argument("-r", "--keep-retry", action="store_true", help="keep retry connect/reconnect LIDAR", default=False)
    parser.add_argument("-u", "--lidar-util", nargs='*', default=None, type=str, help="lidar-util command")
    parser.add_argument("--non-interactive", action="store_true", help="non interactive mode", default=False)
    parser.add_argument("--record-raw", metavar="RECORD_FILE_PREFIX", type=str, default='', help="record-raw")
    parser.add_argument("--record-raw-size", type=int, default=-1, help="record raw file size in MB, default -1")
    parser.add_argument("-y", "--yaml", type=str, default="", help="yaml file path")
    parser.add_argument("--lidarhome", type=str, default="", help="lidarhome")
    parser.add_argument("--backtrace", type=str, default="", help="backtrace file")
    parser.add_argument("--lidar-ip", type=str, default="172.168.1.10", help="lidar ip")
    parser.add_argument("--processed", action="store_true", help="the source is in processed PC format", default=False)
    parser.add_argument("--output", type=str, default="output", help="output dir")
    parser.add_argument("--docker-repo", type=str, default="ivusw/ros-driver-test-public", help="docker repo")
    parser.add_argument("--docker-tag", default="", type=str, help="docker repo tag")
    parser.add_argument("-c", "--reconfigure", default="../..", type=str, help="rqt_reconfigure")
    parser.add_argument("-s", "--show-sample", action="store_true", help="show sample", default=False)
    parser.add_argument("--pcap", type=str, default="", help="play captured file")
    parser.add_argument("--bag", type=str, default="", help="play bag file")
    parser.add_argument("--record-csv-file", nargs="?", type=str, action="store", const="", help="record pointcloud to csv")
    parser.add_argument("--csv-kvp-file", "--kvp-file", type=str, default="", help="kvp file for record csv file")
    parser.add_argument("--csv-speed", type=int, default=5, help="convert to csv file speed (default is 5MB/s)")
    parser.add_argument("--csv-option", type=str, default="", help="options pass to csv convertion")
    parser.add_argument("--multi-return-mode", type=int, default=-1, help="set multi-return mode")
    parser.add_argument("--record-bag-file", nargs="?", type=str, action="store", const="", help="use rosbag to record pointcloud, argument is rosbag options")
    parser.add_argument("-o", "--driver-option", type=str, default="", help="options pass to driver")
    parser.add_argument("--model", type=str, default="", help="model, default is system default (REV_D)")
    parser.add_argument("--unit", type=str, default="", help="unit, default is system default (DB1)")
    parser.add_argument("-l", "--label", type=str, default="", help="lable the container as rosdocer$label, used by exec-docker-bash.bash to find the right container")
    parser.add_argument("--list-tags", metavar="KEY", nargs='*', type=str, default=None, help="list remote tags that contains keys")
    parser.add_argument("--list-tags-local", metavar="KEY", nargs='*', type=str, default=None, help="list local tags that contains keys")
    parser.add_argument("--ros-master", type=str, default="", help="set the ROS_MASTER_URI, to have viewer on a different computer than the one running the ROS driver")
    parser.add_argument("--timer", type=int, default=0, help="timer (in seconds) before terminating; off by default")
    parser.add_argument("--hard-stop", type=int, default=0, help="hard-stop (in seconds) before terminating lidar-util; off by default")
    parser.add_argument("-sf", "--skip-frames", type=int, default=0, help="number of frames that should be ignored at startup (default 0)")
    parser.add_argument("-pf", "--process-frames", type=int, default=0, help="number of frames that should be processed before terminating; unlimited by default")
    args = parser.parse_args()

    script_start = datetime.now().strftime("%Y%b%d-%H%M%S")
    print("Script is starting {}".format(script_start))

    if args.show_sample:
        sample = '''
- see all command line options:
  {cmd} -h

- use ros-kinetic-innovusion-driver-release-1.4.1.1-public.deb, show lidar live data
  {cmd} --deb-file ros-kinetic-innovusion-driver-release-1.4.1.1-public.deb

- use public build, tagged by release-1.0.6, show lidar 172.168.1.10 live data, REV_E unit
  {cmd} --tag release-1.0.6 --lidar-ip 172.168.1.10 --model REV_E

- use public build, tagged by release-1.0.6, automatically download and use the latest proper yaml, show lidar live data, use sample.kvp file
  {cmd} --tag release-1.0.6 -a --kvp-file sample.kvp

- use public build, tagged by release-1.0.5-rc1, use DB1.yaml, show lidar live data
  {cmd} --tag release-1.0.5-rc1 --yaml DB1.yaml

- use public build, tagged by release-1.0.5-rc1, use DB1.yaml, play raw data sample_ADCData.dat, play once at 15MB/s, use reflectance_mode
  {cmd} --tag release-1.0.5-rc1 --yaml DB1.yaml --pcap sample_ADCData.dat -o "read_once:=1 packet_rate:=15 reflectance_mode:=true"

- use internal build, tagged by release-1.0.5-rc1, use DB1.yaml, show lidar live data
  {cmd} --internal-build --tag release-1.0.5-rc1 --yaml DB1.yaml

- use internal build, tagged by release-1.0.5-rc1, collect raw data and save 1GB raw data to file output/db1-sample###
  {cmd} --tag release-1.0.5-rc1 --record-raw db1-sample --record-raw-size 1000 

- use public build, tagged by release-1.0.5-rc1, collect bag file and save to output/
  {cmd} --tag release-1.0.5-rc1 --record-bag-file "--duration 10 --output-prefix DC12_" --yaml db1-sample

- use public build, tagged by release-1.0.5-rc1, to convert raw data to bagfile, record all topics
  {cmd} --tag release-1.0.5-rc1 --yaml DB1.yaml --record-bag-file --pcap sample_ADCData.dat

- use public build, tagged by release-1.0.5-rc1, to convert raw data to bagfile
  {cmd} --tag release-1.0.5-rc1 --yaml DB1.yaml --record-bag-file --pcap sample_ADCData.dat

- replay bagfile
  {cmd} --bag output/sample.bag

- convert raw to csv file
  {cmd} --tag release-1.6.0-rc36 --pcap 22306003-1_ADCData_04_27_2020_15_09_00.dat --yaml unit435.yaml --record-csv-file

NOTE: when using --record-bag-file, use double-dash arguments to pass to rosbag, or start
with a space inside the quotes. Otherwise the argument is handled by this script and *not*
passed along to rosbag.
        '''
        print(sample.format(cmd=sys.argv[0]))
        exit(0)

    if not args.docker_tag:
        if args.ros_version == "kinetic":
            args.docker_tag = "ubuntu1604-kineticjsk"
        elif args.ros_version == "melodic":
            args.docker_tag = "ubuntu1804-melodicjsk"
        elif args.ros_version == "indigo":
            args.docker_tag = "ubuntu1404-indigo"
        else:
            print("ros-version {} is not supported".format(args.ros_version))
            exit(1)

    internal_yaml_file_specified = False
    cannot_use_public = False
    if args.yaml:
        assert os.path.isfile(args.yaml), "{} doesn't exist.".format(args.yaml)
        if 'dynamic_reconfigure.encoding' in open(args.yaml).read():
            internal_yaml_file_specified = True
            print("force to use internal build for internal yaml file");
            args.public_build = False
            cannot_use_public = True

    if args.deb_file:
        if "internal" in args.deb_file:
            use_public_build = False
        else:
            if cannot_use_public:
                print("deb file is not internal build and cannot be use for the task specified.")
                exit(1)
            use_public_build = True
        args.public_build = use_public_build
    else:
        if not args.tag:
            print("Please provide --deb-file or --tag option.")
            exit(0)
        use_public_build = args.public_build

    if args.record_bag_file is not None:
        print("record_bag_file option: {}".format(args.record_bag_file))
        if not args.yaml:
            if args.pcap:
                print("please provide a yaml file to record bag file from pcap")
                exit(1)

    if use_public_build:
        # rqt reconfigure only used for internal build
        args.reconfigure = ""

    if args.pcap and args.bag:
        print("cannot specify --bag and --pcap together")
        exit(1)

    main(args, use_public_build, internal_yaml_file_specified)
