#!/bin/bash
#
# Usage: ./exec-deb-lidar-util.bash <DEB-FILE> <LIDAR_UTIL_ARGS> 

set -e

DEB=$1
shift
ROSVER=$1
shift
args=$@

dpkg --fsys-tarfile ${DEB} | tar xOf - ./opt/ros/${ROSVER}/lib/innovusion_pointcloud/innovusion_lidar_util > innovusion_lidar_util
chmod +x ./innovusion_lidar_util
./innovusion_lidar_util $args

