#!/bin/bash
#
# see https://docs.docker.com/install/linux/docker-ce/ubuntu/#upgrade-docker-ce-1
#

install_docker_ce () {
    set -e
    sudo apt-get remove docker docker-engine docker.io
    sudo apt-get update
    sudo apt-get install -y \
	 apt-transport-https \
	 ca-certificates \
	 curl \
	 software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo apt-key fingerprint 0EBFCD88
    sudo add-apt-repository \
	 "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt-get update
    sudo apt-get install -y docker-ce
}

fix_group () {
    set +e
    sudo groupadd docker
    sudo gpasswd -a $USER docker
    newgrp docker
    docker run hello-world
}

PKG_OK=$(dpkg-query -W --showformat='${Status}\n' docker-ce|grep "install ok installed")
echo Checking for docker-ce: $PKG_OK
if [ "" == "$PKG_OK" ]; then
  echo "No docker-ce. Setting up docker_ce."
  install_docker_ce
fi
fix_group
