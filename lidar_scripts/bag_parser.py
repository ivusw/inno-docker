#!/usr/bin/python

import argparse
import rosbag
from struct import *
from sensor_msgs.msg import PointField
import numpy as np

_DATATYPES = {}
_DATATYPES[PointField.INT8]    = ('b', 1)
_DATATYPES[PointField.UINT8]   = ('B', 1)
_DATATYPES[PointField.INT16]   = ('h', 2)
_DATATYPES[PointField.UINT16]  = ('H', 2)
_DATATYPES[PointField.INT32]   = ('i', 4)
_DATATYPES[PointField.UINT32]  = ('I', 4)
_DATATYPES[PointField.FLOAT32] = ('f', 4)
_DATATYPES[PointField.FLOAT64] = ('d', 8)

topic_iv = 'iv_points'
def _get_struct_fmt(is_bigendian, fields, offset_max, field_names=None):
    fmt = '>' if is_bigendian else '<'
    offset = 0
    for field in (f for f in sorted(fields, key=lambda f: f.offset) if field_names is None or f.name in field_names):
        if offset <= field.offset:
            fmt += 'x' * (field.offset - offset)
            offset = field.offset
            if field.datatype not in _DATATYPES:
                print('Skipping unknown PointField datatype [{}]'.format(field.datatype))
            else:
                datatype_fmt, datatype_length = _DATATYPES[field.datatype]
                fmt    += field.count * datatype_fmt
                offset += field.count * datatype_length
        else:
            print("!!!! offset {} {}".format(offset, fmt))
    fmt += 'x' * (offset_max - offset)
    return fmt


def _get_struct_fmt_map(is_bigendian, fields):
    result = []
    fmt_pre = '>' if is_bigendian else '<'
    for field in sorted(fields, key=lambda f: f.offset):
        if field.datatype not in _DATATYPES:
                print('Skipping unknown PointField datatype [{}]'.format(field.datatype))
                exit(-1)
        else:
            datatype_fmt, datatype_length = _DATATYPES[field.datatype]
            result.append((fmt_pre + datatype_fmt, field.offset, field.name))
    result.sort(key=lambda tup: tup[2])
    return result


def get_ndarray_from_msg(msg, frame_idx=None):
    fields = _get_struct_fmt_map(msg.is_bigendian, msg.fields)
    arrays = []
    frame_col_name = "frame_idx"
    if frame_idx != None:
        fields = [(None, None, frame_col_name)] + fields
    num_points = msg.row_step / msg.point_step
    for f in fields:
        if f[2] != frame_col_name:
            if (num_points > 0):
                arrays.append(np.ndarray((num_points), f[0], msg.data, f[1], (msg.point_step)))
            else:
                arrays.append(np.ndarray((num_points)))
        else:
            arrays.append(np.full((num_points), frame_idx, dtype=np.int32))

    arrays2 = np.swapaxes(arrays, 0, 1)

    return num_points, [f[2] for f in fields], arrays2


def write_csv(csv, frame_idx, num_points, fields, arrays):
    if frame_idx == 0:
        csv.write(",".join([f for f in fields]) + '\n')
    if False:
        np.savetxt(csv, arrays)
    else:
        if (0 == num_points):
            csv.write("{}, n/a, n/a, n/a, n/a, n/a, n/a\n".format(frame_idx))
        else:
            for i in xrange(num_points):
                line = ",".join(map((lambda x: '{:.8f}'.format(x) if (x != int(x)) else str(int(x))) , arrays[i])) + '\n'
                csv.write(line)


def process(args):
    bag_in = rosbag.Bag(args.src)
    csv = open(args.dst, 'w')

    idx = -1
    processed = 0

    for topic, msg, t in bag_in.read_messages(topics=[topic_iv]):
        idx += 1
        if idx < args.start:
            continue
        if args.end and args.end <= idx:
            break

        num_points, fields, arrays = get_ndarray_from_msg(msg, idx)
        write_csv(csv, idx, num_points, fields, arrays)
        processed += 1

    bag_in.close()
    csv.close()

    print("total frames processed {}".format(processed))


def main(args):
    process(args)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--src", type=str, required=True, help="input bag file")
    parser.add_argument("--dst", type=str, default="", help="output csv file")
    parser.add_argument("--topic", type=str, default=topic_iv, help="topic of messages")
    parser.add_argument("--start", type=int, default=0, help="start frame")
    parser.add_argument("--end", type=int, default=0, help="end frame")
    args = parser.parse_args()
    if args.dst == "":
        args.dst = args.src + "_csv"
    main(args)
