#!/bin/bash
if [ "$#" -ne 1 ]; then
    echo "Usage $0 CSV-FILENAME"
    exit 1
fi
f_count=`sort -u -t, -k1,2 $1 | wc -l`
p_count=`cat $1 | wc -l`
echo frames_count=$(( $f_count -1 )) points_count=$(( $p_count -1 ))
