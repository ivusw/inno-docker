#!/usr/bin/python

import argparse
import os
import sys
import rosbag
from struct import *
from std_msgs.msg import Int32, String
from copy import deepcopy
from sensor_msgs.msg import PointCloud2, PointField
from sensor_msgs import point_cloud2
import numpy as np

import bag_parser

default_topic_iv = 'iv_points'

def _get_field_idx(fields, field_name):
    for idx, field in enumerate(f for f in sorted(fields, key=lambda f: f.offset)):
        if field.name == field_name:
            return idx
    assert 0, "cannot find idx for {} from fields {}".format(field_name, fields)


def _get_pointcloud2(msg):
    ps = PointCloud2(header=msg.header,
                     height=1,
                     width=msg.row_step / msg.point_step,
                     is_dense=False,
                     is_bigendian=msg.is_bigendian,
                     fields=msg.fields,
                     point_step=msg.point_step,
                     row_step=msg.row_step,
                     data=msg.data)
    return ps

def filter(args, topic, msg, processed, bag_out, t):
    ps = _get_pointcloud2(msg)
    rd = 180/3.1416
    channel_mask = 1

    # get points array
    cloud_points = list(point_cloud2.read_points(ps, skip_nans=True))

    flag_idx = _get_field_idx(msg.fields, 'flags')
    x_idx = _get_field_idx(msg.fields, 'x')
    y_idx = _get_field_idx(msg.fields, 'y')
    z_idx = _get_field_idx(msg.fields, 'z')
    ts_idx = _get_field_idx(msg.fields, 'timestamp')

    max_xap = 0
    max_yap = 0
    min_xap = 0
    min_yap = 0

    max_xa = 0
    max_ya = 0
    min_xa = 0
    min_ya = 0
    xy_filter = not (args.fov_up is None and args.fov_down is None and args.fov_left is None and args.fov_right is None)
    filtered = 0
    left = 0
    output_points = []
    for counter, value in enumerate(cloud_points):
        l = list(value)
        if args.remove_flag:
            l[flag_idx] = l[flag_idx] & channel_mask
        if args.sanitize:
            l[ts_idx] = 0
            l[flag_idx] = 0
        if xy_filter:
            x = l[x_idx]
            y = l[y_idx]
            z = l[z_idx]
            xa = np.arctan(x/z) * rd
            ya = np.arctan(y/z) * rd
            max_xap = max(max_xap, xa)
            max_yap = max(max_yap, ya)
            min_xap = min(min_xap, xa)
            min_yap = min(min_yap, ya)
            if args.fov_up is not None and xa > args.fov_up:
                filtered += 1
                continue
            if args.fov_right is not None and ya > args.fov_right:
                filtered += 1
                continue
            if args.fov_down is not None and xa < args.fov_down:
                filtered += 1
                continue
            if args.fov_left is not None and ya < args.fov_left:
                filtered += 1
                continue
            max_xa = max(max_xa, xa)
            max_ya = max(max_ya, ya)
            min_xa = min(min_xa, xa)
            min_ya = min(min_ya, ya)
        #cloud_points[counter] = tuple(l)
        output_points.append(tuple(l))
        left += 1

    if xy_filter and filtered:
        print("{} filtered, {} left, [{} {} {} {}] -> [{} {} {} {}]".format(filtered, left, min_xap, min_yap, max_xap, max_yap, min_xa, min_ya, max_xa, max_ya))
    if args.remove_flag:
        print("internal flags removed")
    if args.sanitize:
        output_points = sorted(output_points, key=lambda x: (x[x_idx], x[y_idx]))
        print("sanitized")
    # print(msg.header)
    pc2 = point_cloud2.create_cloud(msg.header, msg.fields, output_points)
    bag_out.write(topic, pc2, t=t)

    print("write frame {}, {} points".format(processed, len(output_points)))


def info(args, topic, msg, processed):
    ps = _get_pointcloud2(msg)

    # get points array
    cloud_points = list(point_cloud2.read_points(ps, skip_nans=True))

    output_points = []
    for counter, value in enumerate(cloud_points):
        l = list(value)
        output_points.append(tuple(l))

    pc2 = point_cloud2.create_cloud(msg.header, msg.fields, output_points)

    print(msg.header)
    print("frame {}, {} points".format(processed, len(output_points)))


def process(args):
    ACT_CSV = 0
    ACT_FILTER = 1
    ACT_INFO = 2
    bag_in = rosbag.Bag(args.src)
    bag_out = None
    csv = None
    if args.subcommand == "write_csv":
        if args.dst == "":
            print("missing --dst argument")
            exit(1)
        csv = open(args.dst, 'w')
        action = ACT_CSV
    elif args.subcommand == "filter":
        if args.dst == "":
            print("missing --dst argument")
            exit(1)
        bag_out = rosbag.Bag(args.dst, 'w')
        action = ACT_FILTER
    elif args.subcommand == "info":
        action = ACT_INFO
    else:
        assert 0, "invalid command {}".format(args.subcommand)

    idx = -1
    processed = 0
    for topic, msg, t in bag_in.read_messages(topics=[args.topic]):
        print("t={}".format(t))
        idx += 1
        if idx < args.start:
            continue
        if args.end and args.end <= idx:
            break

        if (action == ACT_FILTER):
            filter(args, topic, msg, processed, bag_out, t)
        if (action == ACT_INFO):
            info(args, topic, msg, processed)
        elif (action == ACT_CSV):
            num_points, fields, arrays = bag_parser.get_ndarray_from_msg(msg, idx)
            bag_parser.write_csv(csv, idx, num_points, fields, arrays)
        else:
            pass

        processed += 1

    bag_in.close()

    if bag_out is not None:
        bag_out.close()

    if csv is not None:
        csv.close()

    print("total frames processed {}".format(processed))

def main(args):
    process(args)

if __name__ == "__main__":
    epilog = '''
Examples of use:
- convert bag file to csv file
  {cmd} --src source.bag --dst destination.csv write_csv

- filter bag file, remove internal flag and limit the FOV to (-10, 10, -30, 30)
  {cmd} --src source.bag --dst filtered.bag filter --fov-down -10 --fov-up 10 --fov-left -30 --fov-right 30 --remove-flag

- filter bag file, sanitize the bag file, i.e. set flags and point timestamp to 0, also sort the points in messages
  {cmd} --src source.bag --dst sanitized.bag filter --sanitize
'''.format(cmd=sys.argv[0])

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, epilog=epilog)

    parser.add_argument("--src", type=str, required=True, help="input bag file")
    parser.add_argument("--dst", type=str, default="", help="output bag file")
    parser.add_argument("--topic", type=str, default=default_topic_iv,
                        help="topic of messages, default is {}".format(default_topic_iv))
    parser.add_argument("--start", type=int, default=0, help="start frame#, default is 0")
    parser.add_argument("--end", type=int, default=0, help="end frame#, default is 0 (no limit)")

    subparsers = parser.add_subparsers(dest='subcommand')
    parser_csv = subparsers.add_parser('write_csv')
    parser_filter = subparsers.add_parser('filter')
    info_filter = subparsers.add_parser('info')
    parser_filter.add_argument("--remove-flag", action="store_true", default=False, help="remove flag")
    parser_filter.add_argument("--sanitize", action="store_true", default=False, help="sanitize")
    parser_filter.add_argument("--fov-up", type=float, help="FOV up angle")
    parser_filter.add_argument("--fov-down", type=float, help="FOV down angle")
    parser_filter.add_argument("--fov-left", type=float, help="FOV left angle")
    parser_filter.add_argument("--fov-right", type=float, help="FOV right angle")

    args = parser.parse_args()
    if os.path.realpath(args.dst) == os.path.realpath(args.src):
        print("src and dst file cannot be the same")
        exit(1)
    main(args)
