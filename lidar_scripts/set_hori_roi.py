#!/usr/bin/env python
import argparse
import rospy
from std_msgs.msg import Float64

def set_hori_roi(hori_roi_angle):
    pub = rospy.Publisher('hori_roi', Float64, queue_size=10)
    rospy.init_node('set_hori_roi', anonymous=True)
    rospy.sleep(0.2)
    pub.publish(hori_roi_angle)

if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument("-a", "--angle", type=float, default="0.0", help="hori_roi_angle")
        args = parser.parse_args()

        set_hori_roi(args.angle)
    except rospy.ROSInterruptException:
        pass
