# README #

### What is this repository for? ###

* Provide scripts that setup and run the Innovusion ROS Docker

### Requirement ###
* Ubuntu 16.04/18.04
* 7th Intel i5 or higher
* have docker-ce installed
  * if not, please run ./install-docker-ce.bash

### How to launch the Innovusion ROS Docker###
* ./launch.py --deb-file <deb-filename>
* ./launch.py -h (for help message)
* ./launch.py -s (for usage example)

### How to conver RAW data to CSV file or BAG file ###
Pleae see HOWTO_CONVERT.txt.
