#!/bin/bash
#
# Usage: ./exec-docker-bash.bash [LABEL]
#        LABEL is the label specified by -l in launch-docker.py
set -e

args=$@

get_docker_id () {
    CNT_ID=`docker ps -a -f label=rosdocker -q`
}

get_docker_id
if [ -n "${CNT_ID}" ]
then
    docker exec -it `docker ps -a -f label=rosdocker -q` bash -c "source /opt/ros/\$ROS_DISTRO/setup.bash && cd \$IV_CWD && /opt/ros/\$ROS_DISTRO/lib/innovusion_pointcloud/innovusion_lidar_util ${args}"
else
    # echo Have to lauch docker first, while take several seconds ...
    # ./launch-docker.py --tag release-1.0.7 --lidar-util "${args}"
    exit 2
fi
