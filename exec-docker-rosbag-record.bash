#!/bin/bash
#
# Usage: ./exec-docker-bash.bash [LABEL]
#        LABEL is the label specified by -l in launch-docker.py

docker exec -it `docker ps -a -f label=rosdocker$1 -q` bash -c "source /opt/ros/\$ROS_DISTRO/setup.bash; cd /root/output; rosbag record iv_points"
